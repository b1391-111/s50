import React from 'react';

import HighlightCard from './HighlightCard';

// react-bootstrap components
import { 
    Col,
    Container,
    Row, 
} from 'react-bootstrap';

const HighLights = () => {
    return (
        <Container fluid>
            <h1 className="text-center">Highlights</h1>
            <Row>
                <Col>
                    <HighlightCard 
                        photo="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTrKHPsvNDJHY9tWpkHrfkfo8Dkf0LvZU3Hdg&usqp=CAU"
                        title="Study Now, Pay Later"
                        desc="Some quick example text to build on the card title and make up the bulk of the card's content."
                    />
                </Col>
                <Col>
                    <HighlightCard 
                        photo="https://images.unsplash.com/photo-1612151855475-877969f4a6cc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8aGQlMjBpbWFnZXxlbnwwfHwwfHw%3D&w=1000&q=80"
                        title="Learn from Home"
                        desc="Some quick example text to build on the card title and make up the bulk of the card's content."
                    />
                </Col>
                <Col>
                    <HighlightCard 
                        photo="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Convex_lens_%28magnifying_glass%29_and_upside-down_image.jpg/341px-Convex_lens_%28magnifying_glass%29_and_upside-down_image.jpg"
                        title="Be Part of Our Community"
                        desc="Some quick example text to build on the card title and make up the bulk of the card's content."
                    />
                </Col>
            </Row>
        </Container>
    )
}

export default HighLights;