import React, { useState, useEffect } from 'react';

// react-bootstrap component
import { Button, Card } from 'react-bootstrap';

const CourseCard = ({title, desc, price}) => {
    const [enrollees, setEnrollees] = useState(0);
    const [seats, setSeats] = useState(10);
    const [isDisabled, setIsDisabled] = useState(false);

    const thousands_separators = (num) => {
        let num_parts = num.toString().split(".");
        num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return num_parts.join(".");
    }

    const handleEnrollees = () => {
        if(seats > 0){
            setEnrollees(enrollees + 1);
            setSeats(seats - 1);
        }
    }

    useEffect(() => {
        if(seats === 0) {
            setIsDisabled(true);
        }
    }, [seats])

    return (
        <Card
            className="text-left"
        >
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Subtitle className="mb-2 text-dark">
                    Description: 
                    <p> {desc} </p>
                </Card.Subtitle>
                <Card.Text>
                    Enrollees: {enrollees}
                </Card.Text>
                <Card.Text>
                    Available Seats: {seats}
                </Card.Text>
                <Card.Text>
                    Price:
                    <Card.Text>PHP {thousands_separators(price)}</Card.Text>
                </Card.Text>
                <Button
                    onClick={handleEnrollees}
                    disabled={isDisabled}
                    className="btn-info"
                >
                    Enroll
                </Button>
            </Card.Body>
        </Card>
    )
}

export default CourseCard;
