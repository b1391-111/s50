import React, { useState } from 'react';

// React Context
import { UserProvider } from './UserContext';

// CSS
import './App.css';

// react-router
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

// Components
import Navbar from './components/Navbar/Navbar';
import Banner from './components/Banner/Banner';
import Register from './pages/Register';
import Courses from './pages/Courses';
import Logout from './pages/Logout';
import Login from './pages/Login';
import Home from './pages/Home';


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });
  // React Context - is nothing but a global state to the app. It is a way to make a particular data available to all the components no matter how they are nested. Context helps you broadcast the data and changes happening to that data, to all components.
  // const [user, setUser] = useState({
  //   email: localStorage.getItem("email")
  // });

  // function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Navbar />    
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>

          <Route path="/courses">
            <Courses />
          </Route>

          <Route path="/login">
            <Login />
          </Route>

          <Route path="/register">
            <Register />
          </Route>

          <Route path="/logout" component={Logout} />
          
          <Route path="/*">
            <Banner 
              title="404 - Page Not Found!"
              content="We cannot find the page that you are looking for."
              dest="/"
              btnLabel="Back to homepage"
            />
          </Route>
        </Switch>
      </Router>
    </UserProvider>
  );
}

export default App;