import React, { useContext, useEffect } from 'react';

// user context
import UserContext from '../UserContext';

// React router
import { Redirect } from 'react-router-dom';

const Logout = () => {
    const { setUser, unsetUser } = useContext(UserContext);
    unsetUser();
    useEffect(() => {
        setUser({email: null});
    }, []);
    alert("Logging out!");

    return (
        <Redirect to="/login"/>
    )
}

export default Logout;